var global_num = 12;
var Numbers = /** @class */ (function () {
    function Numbers() {
        this.num3 = 13;
    }
    Numbers.prototype.storeNum = function () {
        var local_num = 15;
        console.log("Local variable = " + local_num);
    };
    Numbers.sval = 10;
    return Numbers;
}());
console.log("Global variable = " + global_num);
console.log("Static variable = " + Numbers.sval);
var obj = new Numbers();
console.log("Class variable = " + obj.num3);
